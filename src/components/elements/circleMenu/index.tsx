import React from "react";
import {Motion, spring} from 'react-motion';
import './styles.css';
import menuitems from './menuitems.json'


const calculatePositionX = (radius: number, circleNumber: number, amountOfCircles: number) => {
    const totalDegreesOfAngles = 90;
    const eachDegree = totalDegreesOfAngles / (amountOfCircles-1);
    const degreeThisCircle = eachDegree * circleNumber;
    return radius * Math.cos(degreeThisCircle * Math.PI / 180);
};

const calculatePositionY = (radius: number, circleNumber: number, amountOfCircles: number) => {
    const totalDegreesOfAngles = 90;
    const eachDegree = totalDegreesOfAngles / (amountOfCircles-1);
    const degreeThisCircle = eachDegree * circleNumber;
    return -(radius * Math.sin((degreeThisCircle * Math.PI / 180))) ;
};

const calculateCircleDiameter = (amountOfCircles: number) => {
    return 15/amountOfCircles;
};

interface SubCircleProps {
    key: number;
    radius: number;
    circleNumber: number;
    amountOfCircles: number;
    content: ButtonItem;
}



const SubCircle = (props: SubCircleProps) => {
    const {radius, circleNumber, amountOfCircles, content} = props;
    const click = () => {
        window.location.href = content.link;
    };
    const selfDiameter = calculateCircleDiameter((amountOfCircles));
    const selfRadius = selfDiameter/2;
    return (
        <div onMouseDown={click} className='circle'  style={{
            WebkitTransform: `translateX(${calculatePositionX(radius, circleNumber, amountOfCircles)+5/2-selfRadius}rem) translateY(${calculatePositionY(radius, circleNumber, amountOfCircles)+5/2-selfRadius}rem)`,
            transform: `translateX(${calculatePositionX(radius, circleNumber, amountOfCircles)+5/2-selfRadius}rem) translateY(${calculatePositionY(radius, circleNumber, amountOfCircles)+5/2-selfRadius}rem)`,
            width: `${selfDiameter}rem`, height: `${selfDiameter}rem`
        }}> <img  width='80%' src={content.iconUrl} className="buttonContent unselectable pics" title={content.text}  alt={content.text}/></div>
    )
};


interface SubCircleManagerProps {
    radius: number;
    contents: Array<ButtonItem>;
}

const SubCircleManager = (props: SubCircleManagerProps) => {
  const {radius, contents} = props;


  return (
      <div>
          {contents.map((content: any, index: number) => {
              return <SubCircle amountOfCircles={contents.length} circleNumber={index} radius={radius} content={content} key={index} />
          })}
      </div>
  )
};

interface ButtonItem {
    link: string;
    text: string;
    iconUrl: string;
}


export default function CircleMenu() {
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(!open);
    };


    const buttons: Array<ButtonItem> = menuitems;

    return (
        <div className="CircleMenu">
            <Motion style={{radius: spring(open ? 10 : 0)}}>
                {({radius}) =>
                    <div>
                        <SubCircleManager radius={radius} contents={buttons}/>
                        <div className="circle" onMouseDown={handleClick} style={{
                            height: `5rem`,
                            width: `5rem`,
                        }}><span className="buttonContent unselectable">menu</span></div>
                    </div>
                }
            </Motion>
        </div>
    )
}