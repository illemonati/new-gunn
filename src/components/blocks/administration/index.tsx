import React from "react";
import CircleMenu from "../../elements/circleMenu";
import '../../../App.css';

export default function Administration() {

    return (
        <div className="MainPage">
            <h1 className="textCenter">Gunn is A Great School!</h1>
            <CircleMenu />
        </div>

    )
}
