import React, {useState} from "react";
import {Box, Paper, styled, Typography} from "@material-ui/core";
import {Grid} from "@material-ui/core";
import CircleMenu from "../../elements/circleMenu";
import '../../../App.css';
import schedule from './schedule.json';
import BlockColors from './block-colors.json';
import {PaperProps} from "@material-ui/core/Paper";
import CircleColorSchemeChooser from "./CircleColorSchemeChooser/circleColorSchemeChooser";
import {getSavedColorScheme} from "./util";
type ColoredPaperProps = PaperProps & {backgroundcolor: string};

const ColoredPaper = styled(Paper)({
    backgroundColor: props => props.backgroundcolor
}) as React.ComponentType<ColoredPaperProps>;


class Time {
    hour: number;
    min: number;
    constructor(jsonTime: string) {
        this.hour = parseInt(jsonTime.slice(0, 2));
        this.min = parseInt(jsonTime.slice(2, 4));
    }
    toString() : string {
        return this.hour.toString().padStart(2, '0') + ':' + this.min.toString().padStart(2, '0');
    }
}

const getColor = (blockName: string, blockColors: any): string => {
    for (let key of Object.keys(blockColors)) {
        if (key === blockName) {
            // @ts-ignore
            return blockColors[key];
        }
    }
    return 'grey';

};

const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];


const BellSchedule = () => {
    let bc = {};
    try {
        //@ts-ignore
        bc = BlockColors[getSavedColorScheme()];
    } catch (e) {
        bc = BlockColors['default'];
    }

    const [blockColors, setBlockColors] = useState(bc);

    const setColorScheme = (colorScheme: Object) => {
        // @ts-ignore
        setBlockColors(colorScheme);
    };


    const textStyle = {
        fontSize: '1.25vw'
    };
    return (
        <div className="BellSchedule page">
            <Box height='100%'>
                <h1>Bell Schedule</h1>
                <Box className='scheduleMain' maxWidth='80%' margin='auto'>
                    {/*<img src={"https://gunn.pausd.org/sites/default/files/19-20-bell-schedule-8-13.gif"}  alt="schedule"/>*/}
                    <Grid container spacing={1} justify='center'>
                        {schedule.map((day, i) => {
                            return (
                                <React.Fragment key={i}>
                                    <Grid item xs={2}>
                                        <Typography variant='h4' style={{
                                            fontSize: '2.5vw'
                                        }}>{weekdays[i]}</Typography>
                                        <br />
                                        <Grid container spacing={1} justify='center' direction='column'>
                                            {/* eslint-disable-next-line array-callback-return */}
                                            {day.map((block, j) => {
                                                const begin = new Time(block.begin).toString();
                                                const end = new Time(block.end).toString();
                                                const time = `${begin} - ${end}`;
                                                if(block.name !== "Passing"){
                                                return (
                                                    <Grid item  xs key={j}>
                                                        <ColoredPaper backgroundcolor={getColor(block.name, blockColors)}>
                                                            <Typography style={textStyle}>{(block.name==='AfterSchool') ? block.actualName : block.name}</Typography>
                                                            <Typography style={textStyle}>{time}</Typography>
                                                        </ColoredPaper>
                                                    </Grid>
                                                )}
                                            })}
                                        </Grid>
                                    </Grid>
                                </React.Fragment>
                            )
                        })}
                    </Grid>
                </Box>
            </Box>
            <CircleMenu />
            <CircleColorSchemeChooser colorSchemes={BlockColors} setColorScheme={setColorScheme}/>
        </div>
    )
};

export default BellSchedule;

