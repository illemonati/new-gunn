import React, {useState} from "react";
import './CircleColorSchemeChooser.css';
import {Drawer, Grid, Paper, Typography} from "@material-ui/core";
import {setSavedColorScheme} from "../util";




interface CircleColorSchemeChooserProps {
    colorSchemes: Object,
    setColorScheme: (colorScheme: object) => any
}

const CircleColorSchemeChooser = (props: CircleColorSchemeChooserProps) => {



    const [drawerOpen, setDrawerOpen] = useState(false);

    const handleColorSchemeSelect = (colorSchemeName: string) => {
        // @ts-ignore
        props.setColorScheme(props.colorSchemes[colorSchemeName]);
        setSavedColorScheme(colorSchemeName);
    };


    return (
        <div className="CircleColorSchemeChooser">
            <div className="openSchemeChooserButton" onClick={() => setDrawerOpen(!drawerOpen)}>
                <Typography variant='h5'>Color Scheme</Typography>
            </div>

            <Drawer anchor="right" open={drawerOpen} onClose={() => {setDrawerOpen(!drawerOpen)}}>
                <div style={{width: "20vw", textAlign: "center", overflow: "hidden", overflowY: "scroll", scrollbarWidth: "none"}}>
                    <Typography variant="h4">Color Schemes</Typography>
                    <Typography variant="caption">Click to change color scheme</Typography>
                    <br />
                    <Grid container spacing={3} justify='center'>
                    {Object.keys(props.colorSchemes).map((colorSchemeName: string, i) => {
                        return (
                            <Grid item style={{width: "100%"}} key={i}>
                                {/*
                                //@ts-ignore */}
                                <Paper onClick={() => {handleColorSchemeSelect(colorSchemeName)}}>
                                    <Typography variant="h5" style={{backgroundColor: 'grey'}}>{colorSchemeName}</Typography>
                                    {/*
                                    // @ts-ignore */}
                                    {Object.keys(props.colorSchemes[colorSchemeName]).map((blockName, j) => {
                                        // @ts-ignore
                                        const color = props.colorSchemes[colorSchemeName][blockName];
                                        return (
                                            <div style={{backgroundColor: color}} key={j}>
                                               <Typography variant="body1" style={{
                                                   textAlign: "left",
                                                   marginLeft: "10%"
                                               }} >{blockName}
                                                 <span style={{
                                                     float: "right",
                                                     marginRight: "10%"
                                                 }}>
                                                     {color}
                                                 </span>
                                               </Typography>
                                            </div>
                                        )
                                    })}
                                </Paper>
                            </Grid>
                        )
                    })}
                    </Grid>
                </div>
            </Drawer>
        </div>
    )
};

export default CircleColorSchemeChooser;


