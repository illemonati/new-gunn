
export const getSavedColorScheme = () : string => {
    const color = localStorage.getItem("savedBellScheduleColorScheme");

    if (!color) {
        return "default";
    }

    return color!;

};

export const setSavedColorScheme = (colorSchemeName: string) : void => {
    localStorage.setItem("savedBellScheduleColorScheme", colorSchemeName);
};

