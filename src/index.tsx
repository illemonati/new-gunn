import React from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter as Router } from 'react-router-dom'
import './index.css';
import App from './App';
import BellSchedule from "./components/blocks/bellSchedules";
import EventCalender from "./components/blocks/calender";
import * as serviceWorker from './serviceWorker';
import Administration from "./components/blocks/administration";
import Others from "./components/blocks/others";
import Student from "./components/blocks/student";
import Studies from "./components/blocks/studies";


const routing = (
    <Router basename={process.env.PUBLIC_URL}>
        <div>
            <Route exact path="/" component={App} />
            <Route path="/home" component={App} />
            <Route path="/schedule" component={BellSchedule} />
            <Route path="/calender" component={EventCalender} />
            <Route path="/administration" component={Administration} />
            <Route path="/others" component={Others} />
            <Route path="/student" component={Student} />
            <Route path="/studies" component={Studies} />
        </div>
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
